const express = require("express");
const bodyParser = require("body-parser");

const router = express.Router();

const controller = require('../controller/index-controller');

router.get("/", controller.home);

module.exports = router;