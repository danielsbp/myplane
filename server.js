const Express = require('express');

const app = Express();

const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const router = Express.Router();

//Conexão com o servidor mongodb
mongoose.connect("mongodb://127.0.0.1:27017/myplan")
	.then(x => {
		console.log("Conectado ao servidor!");
	})
	.catch(e => {
		console.log("Falha na conexão!");
	});


//Configurando body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));


// Carregando models:
const User = require('./models/user');
const Task = require('./models/task');
// Carregando rotas:
const signupRoutes = require("./routes/signup");
const indexRoute = require("./routes/index");
const loginRoutes = require("./routes/login");
const todoRoutes = require("./routes/todo");

// Definindo nome das rotas na url
app.use("/signup", signupRoutes);
app.use("/", indexRoute);
app.use("/login", loginRoutes);
app.use("/todo", todoRoutes);

app.listen(8081, function() {
	console.log("Funcionando!");
});