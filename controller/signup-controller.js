const mongoose = require("mongoose");

const User = require("../models/user");

exports.get = (req, res, next) => {
	res.status(200).send({
		title: "Tela de criação de conta",
		description: "Salvar usuário novo no banco de dados"
	});
}

exports.signup = async(req, res, next) => {
	const data = req.body;
	const user = new User(data);
	
	// Salva o usuário que foi criado no banco de dados
	try {
		await user.save()
		res.status(201).send({
			message: "Usuário criado!",
			date: Date.now,
			username: req.body.username
		});	
	} catch(e) {
		res.status(500).send({
			message: "Erro ao criar o usuário!",
			error: e
		});
	}
}