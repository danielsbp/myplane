# MyPlane

Criador de cronogramas e esquemas de organização que visa a simplicidade.

## Metas de funções para o programa:

**Back-end API:**
- [ ] Sistema de Login e Registro
- [ ] To-do list
- [ ] Notificações
- [ ] Controle de Hábitos
- [ ] Planejador diário
- [ ] Integração com emojis

**Front-end:**

Desktop:
- Em breve

Mobile:
- Em breve

Web:
- Em breve
