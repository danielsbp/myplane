const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema({
	name: {
		type: String,
		required: true
	},
	description: {
		type: String
	},
	active: {
		type: Boolean,
		required: true,
		default: true
	},
	status: {
		type: String,
		enum: ["Não feito", "Fazendo", "Feito"],
		required: true,
		default: "Não feito"
	},
	taskType: {
		type: String,
		enum: ["todo", "habit", "day"],
		required: true
	},
	refUser: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "User",
		required: true
	},
	dateAndTime: {
		type: Date,
		required: true,
		default: Date.now
	}
})

module.exports = mongoose.model("Task", schema);