const Task = require("../models/task");
const taskCreator = require("./task-creator");

exports.create = async(req, res, next) => { 
	/* Corpo da requisição POST 
		{
			"name": "Estudar",
			"description": "Estudar javascript com node.js",
			"active": true,
			"status": "Não feito",
			"taskType": "todo",
			"refUser": "60fc5d6b91b6fd3d26b784aa",
			"dateAndTime": new Date("October 13, 2014 11:13:00")
		}
	*/
	let data = req.body;
	taskCreator(data, req, res, next);
	
}


exports.update = async(req, res, next) => {
	/*

	Corpo da requisição PUT
	{
		"id": "60fdff83264d8f72d8905b18",
		"name": "Nome da Tarefa",
		"description": "Descrição da Tarefa",
		"active": true,
		"dateAndTime": "October 13, 2014 11:13:00"
	}

	*/
	let data = req.body;

	await Task.findByIdAndUpdate(data.id, {
		$set: {
			name: data.name,
			description: data.description,
			active: data.active,
			dateAndTime: data.dateAndTime
		}
	});

	res.status(200).send({
		status: "Task alterada com sucesso!"
	});
}


exports.delete = async(req, res, next) => {
	await Task.findOneAndRemove(req.body.id);
	res.status(200).send({
		status: "Task excluida com sucesso!"
	});
}


/*
exports.get() = (req, res, next) => {

}
*/