const express = require("express");
const bodyParser = require("body-parser");

const router = express.Router();

const controller = require('../controller/login-controller');

router.get("/", controller.login);
router.post("/", controller.confirm);

module.exports = router;