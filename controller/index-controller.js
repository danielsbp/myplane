exports.home = (req, res, next) => {
	res.status(200).send({
		name: "MyPlan API",
		version: "0.0.1"
	});
}