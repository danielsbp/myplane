const user = require("../models/user");

exports.login = (req, res, next) => {
	res.status(200).send({
		title: "Tela de login",
		message: "Consumo da tela de login pela API"
	});
}

exports.confirm = async(req, res, next) => {
	//Confirmação de login - Libera o acesso ao usuário a entrar no
	//software por qualquer front-end caso ele passe as informações
	//de acesso corretas.

	let data = await user.findOne({
		user: req.body.user
	});

	// Debug
	//console.log("Dados: "+ data.user);

	function errorMessage(err) {
		res.status(201).send({
			status: "Erro",
			error: err
		});
	}
	if(data){
		let validLogin = data.user == req.body.user && data.password == req.body.password;
		
		if(validLogin) {
			res.status(201).send({
				status: "Usuário logado com sucesso!"
			});
		}
		else {
			errorMessage("Senha inválida!");
		}
	}
	else {
		errorMessage("E-mail ou usuário invalido!");
	}

	//res.status(200).send(data);
}