const Task = require("../models/task");

async function create(data, req, res, next) {
	let task = new Task(data);
		
	try {
		await task.save();
		res.status(201).send({
			status: "Task criada com sucesso!",
			user: req.body.refUser,
			date: Date.now
		});
	} catch(e) {
		res.status(500).send({
			status: "Erro ao criar a Task",
			err: e
		});
	}
}

module.exports = create;
