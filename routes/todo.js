// Manipula o documento "Task"

const express = require("express");
const bodyParser = require("body-parser");

const router = express.Router();

const controller = require('../controller/todo-controller');

router.post("/", controller.create);
router.put("/", controller.update);
router.delete("/", controller.delete);

module.exports = router;