const express = require("express");
const bodyParser = require("body-parser");

const router = express.Router();

const controller = require('../controller/signup-controller');

router.get("/", controller.get);
router.post("/", controller.signup);

module.exports = router;